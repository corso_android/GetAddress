package com.example.getaddress;



public class ResponseAddress {

    private String ip;

    public ResponseAddress(String ip) {
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
